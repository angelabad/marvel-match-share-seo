var UrlPattern = require('url-pattern')
var axios = require('axios')
var URL = require('url').URL;

function getHero (id) {
  return new Promise((resolve, reject) => {
    let hero = []
    axios.get(process.env.GETTING_URL + '/v1/public/characters/' + id)
      .then(response => {
        hero = {
          id: response.data.data.results[0].id,
          name: response.data.data.results[0].name,
          description: response.data.data.results[0].description,
          thumbnail: response.data.data.results[0].thumbnail,
          comicsAvailable: response.data.data.results[0].comics.available
        }
      })
      .then(response => {
        resolve(hero)
      })
      .catch((error) => reject(error))
  })
}

function convertToHttps (data) {
  let url = new URL(data)
  url.protocol = 'https:'
  return url.href
}

exports.handler = async (event, context) => {
  try {
    var pattern = new UrlPattern('*(/:hero1Id)(/:hero2Id)')
    const result = pattern.match(event.path)

    let hero1 = null
    let hero2 = null

    await Promise.all([getHero(result.hero1Id), getHero(result.hero2Id)])
      .then(values => {
        hero1 = values[0]
        hero2 = values[1]
      })

    const title = 'MarvelMatch: ' + hero1.name + ' vs ' + hero2.name
    const description = 'See all the Marvel comics where ' + hero1.name + ' and ' + hero2.name + ' appear together.'
    const url = process.env.OWN_URL + '/match/' + hero1.id + '/' + hero2.id
    const redirUrl = process.env.SHARING_URL + '/match/' + hero1.id + '/' + hero2.id
    const image = convertToHttps(hero1.thumbnail.path + '/landscape_incredible.' + hero1.thumbnail.extension)

    var body = '<!DOCTYPE html><head>\n'
    body += '<title>' + title + '</title>\n'
    body += '<meta property="og:title" content="' + title + '">\n'
    body += '<meta property="og:description" content="' + description + '">\n'
    body += '<meta property="og:site_name" content="MarvelMatch">\n'
    body += '<meta property="og:type" content="website">\n'
    body += '<meta property="og:url" content="' + url + '">\n'
    body += '<meta property="og:image" content="' + image + '">\n'
    body += '<meta property="og:image:width" content="464">\n'
    body += '<meta property="og:image:height" content="261">\n'
    body += '<meta property="og:image:alt" content="' + title + '">\n'
    body += '<meta property="twitter:card" content="summary_large_image">\n'
    body += '<meta property="twitter:title" content="' + title + '">\n'
    body += '<meta property="twitter:site" content="' + url + '">\n'
    body += '<meta property="twitter:description" content="' + description + '">\n'
    body += '<meta property="twitter:creator" content="@matchmarvel">\n'
    body += '<meta property="twitter:img:src" content="' + image + '">\n'
    body += '<link rel="icon" href="https://marvelmatch.com/favicon.png">\n'
    body += '</head><body>\n'
    body += '<script>window.location="' + redirUrl + '";</script>'
    body += '</body></html>'

    return {
      statusCode: 200,
      body: body
    }
  } catch (err) {
    console.log(err)
    return {
      statusCode: 500,
      body: err.message
    }
  }
}
